 const txtFirstName = document.querySelector('#txt-first-name');
 const txtLastName = document.querySelector('#txt-last-name');
 const spanFullName = document.querySelector('#span-full-name');
 const spanGreetings = document.querySelector('.span-greetings');

// alternative for document.querySelector()
/*
        document.getElement('txt-first-name');
        document.getElementByClassName('txt-inputs');
        document.getElementByTagName('input');
*/



const updateFullName = () => {
    const firstName = txtFirstName.value;
    const lastName = txtLastName.value;
    
    spanFullName.innerHTML = firstName + ' ' + lastName;
  }
  
  txtFirstName.addEventListener('keyup', updateFullName);
  txtLastName.addEventListener('keyup', updateFullName);
